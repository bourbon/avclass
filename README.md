# taxAV

Given a set of AV signatures, associate a malware sample to a specific group or 
family.

### Name meaning

Biologists, specifically Taxonomists, group organisms into 
[taxa](https://en.wikipedia.org/wiki/Taxon).  Since the security community 
borrowed "virus" from biology, it seemed fitting to name this project: "taxAV". 
A taxa of the AV signatures given to malware.

## How it works

1. `taxAV` tokenizes a malware signature.  Take the ZeuS trojan (commonly 
referred to as "zbot").  ESET-NOD32 gives the signature "Win32/Spy.Zbot.ACZ".  
Tokenizing this we get `['win32', 'spy', 'zbot', 'acz']`.  Most of these 
strings, by themselves, are fairly useful but not when trying to group multiple 
samples into groups.

2. After tokenization, we want to remove things that are simply too generic.  
When, organizing malware by family name, `win32`, `spy`, and `acz` don't mean 
much.  We're after `zbot`, so we remove the generic with by filtering those 
tokens out.

3. Now that `taxAV` has removed the generic "fluff", we want to "dereference" 
known aliases to the most commonly used name.  "ZBot" is known by various names 
but is commonly referred to as "ZeuS", "ZBot", and "zbocheman".

4. Finally, `taxAV` simply uses 
[Frequency Analysis](https://en.wikipedia.org/wiki/Frequency_analysis) to 
determine the most used token(s) for that sample.
